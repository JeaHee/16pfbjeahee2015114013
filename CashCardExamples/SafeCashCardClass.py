# -*- coding:utf-8 -*-
#상위 클래스 이름이 너무 길어져서 from inport as적용
from CashCardClass import SimpleCashCard as CashCard
#

class SafeCashCard(CashCard):
    #
    def withdraw(self,amount_won):
        """
        SafeCashCard withdraw method
        Check balance before withdraw
        """
        print("SafeCashCard withdraw()") #함수 호출 표식
        #
        if self.check_balance() >= amount_won:
            #
            CashCard.withdraw(self, amount_won)

        else:
            #
            print("**오류 발생**")
            print("잔고가 부족합니다")
            print("인출되지 않았습니다")
#SafeCashCard 클래스 정의 끝

#아래의 내용은 이 파일이 import 될 때는 실행되지 않음
if "__main__"==__name__:
    #CashCard User 모듈의 msg_int 함수를 사용할 수 있게 함
    from CashCard_user import chk_bal

    myCard = CashCard()
    #
    mySafeCard = SafeCashCard()

    #
    mySistersSafeCard = SafeCashCard()

    #
    myCard.deposit(10000)
    #
    mySafeCard.deposit(10000)
    mySistersSafeCard.deposit(200000)

    chk_bal("myCard 입금 후 잔고 확인", myCard)
    chk_bal("mySafeCard 입금 후 잔고 확인", mySafeCard)
    chk_bal("mySistersSafeCard 입금 후 잔고 확인", mySistersSafeCard)
    #

    myCard.withdraw(100000)
    mySafeCard.withdraw(100000)
    mySistersSafeCard.withdraw(100000)

    chk_bal("myCard 출금 후 잔고 확인", myCard)
    chk_bal("mySafeCard 출금 후 잔고 확인", mySafeCard)
    chk_bal("mySistersSafeCard 출금 후 잔고 확인", mySistersSafeCard)
    #