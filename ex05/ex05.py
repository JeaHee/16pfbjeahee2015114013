name = 'zed A. shaw'
my_age = 21 # not a lie
my_height_cm = 170 #cm
my_weight_kg = 45 #kg
my_eyes = 'black'
my_teeth = 'white'
my_hair = 'black'

print "Let's talk about %s" % name
print "He's %d cm tall" % my_height_cm
print "He's %d kg heavy" % my_weight_kg
print "Actually that's not too heavy"
print "He's got %s eyes and %s hair" % (my_eyes, my_hair)
print "His teeth are usually %s depinding on the coffee" % my_teeth

# this line is tricky, try to get it exactly right
print "If I add %d, %d, and %d I get%d" % (my_age, my_height_cm, my_weight_kg, my_age + my_height_cm + my_weight_kg)