# -*- coding:utf-8 -*-
from SafeCashCardClass import SafeCashCard
#입출금 시간을 기록하기 위해 time 모듈을 도입
import time


#
class HistoryCashCard(SafeCashCard):
    """
    Cash card capable of recording histories
    """ #<- dostring

    def __init__(self):
        print("HistoryCashCard __init__()")
        #

        SafeCashCard.__init__(self)
        #

        self.history = []

    #입출금 기록 기능을 추가하기 위해 입금 출금 메소드 재정의
    def deposit(self, amount_won):
        """
        HistoryCashCard deposit method
        deposit amount & add record to history
        """
        print("HistoryCashCard deposit()")
        #입금 기능 자체는 상위 클래스에 이미 정해진 바를 따른다
        SafeCashCard.deposit(self, amount_won)
        # 또는
        #

        self.record_history('deposit', amount_won)

    #
    def withdraw(self, amount_won):
        """
        HistoryCashCard withdraw method
        withdraw amount & add record to history
        """
        print("HistoryCashCard withdraw()")
        #
        SafeCashCard.withdraw(self, amount_won)

        #
        self.record_history('withdraw', amount_won)

    def record_history(self, activity, amount_won):
        record = {
            'time' : time.localtime(),
            'balance' : self.check_balance(),
            'activity' : activity,
            'amount' : amount_won,
        }
        self.history.append(record)

    def show_history(self):
        """
        HistoryCashCard show hisroty method
        show appended history
        """
        print("HistoryCashCard show_history()") #

        #

        print('%25s %10s %10s %10s' %('time and date', 'activity', 'amount', 'balance'))
        #
        for record in self.history:
            #
            print('%25s %10s %10s %10s' %(
                'time and date', 'activity', 'amount', 'balance'))
            #
            for record in self.history:
                #
                print('%25s %10s %10d %10d' % (time.asctime(record['time']),
                                               record['activity'], record['amount'], record['balance']))
                #사용내역 루프 끝

if "__main__" ==__name__:
    print("main 객체 생성". ljust(60, '*'))
    myHistCard = HistoryCashCard()
    print("main 10000원 입금".ljust(60, '*'))
    myHistCard.deposit(10000)
    print("main 9000원 출금".ljust(60, '*'))
    myHistCard.withdraw(9000)
    print("main 내역 확인".ljust(60, '*'))
    myHistCard.show_history()
    print("main 끝".ljust(60, '*'))