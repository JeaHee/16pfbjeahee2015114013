# -*- coding:utf-8 -*-
#http://learnpythonthehardway.org/book/ex15.html
from sys import argv

script, filename = argv

txt = open(filename)

print "Here's your file %r:" % filename
print txt.read()

print "Type the filename again:"
file_again = raw_input("> ")

txt_again = open(file_again)

print txt_again.read()
#실행시 ex15_sample.txt에 내용이 출력되고 다시 파일이름을 입력시 또 출력된다